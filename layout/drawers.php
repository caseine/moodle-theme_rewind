<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

global $CFG, $COURSE, $PAGE;

// Remove the "Competencies" navigation node if no competencies are setup within course, and user cannot manage them.
if ($PAGE->has_secondary_navigation()
        && $PAGE->secondarynav->get('competencies') !== false
        && !has_capability('moodle/competency:coursecompetencymanage', context_course::instance($COURSE->id))
        && empty(core_competency\api::list_course_competencies($COURSE->id))) {
    $PAGE->secondarynav->get('competencies')->remove();
}

require($CFG->dirroot . '/theme/boost/layout/drawers.php');
